
@login
Feature: Login
  User want to login in Secondhand app

  @TC_LO001
  Scenario: TC_LO001 - User want to login with incorrect credential
  Then User click button account
  Then User click button signin
  Then User input incorrect email
  Then User input incorrect password
  Then User click on button login
  
  @TC_LO002
  Scenario: TC_LO002 - User want to login with correct credential
  Then User tap button back and tap button signin
  Then User input correct email
  Then User input correct password
  Then User click button login
  