package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class login {
	@Then("User click button account")
	public void user_click_button_account() {
		Mobile.tap(findTestObject('Object Repository/Login/button_akun'), 0)
	}

	@Then("User click button signin")
	public void user_click_button_signin() {
		Mobile.tap(findTestObject('Object Repository/Login/Button_Masuk'), 0)
	}

	@Then("User input incorrect email")
	public void user_input_incorrect_email() {
		Mobile.setText(findTestObject('Object Repository/Login/fill_email'), 'zaby@getnada.com', 0)
	}

	@Then("User input incorrect password")
	public void user_input_incorrect_password() {
		Mobile.setText(findTestObject('Object Repository/Login/fill_password'), '12345', 0)
	}

	@Then("User click on button login")
	public void user_click_on_button_login() {
		Mobile.tap(findTestObject('Object Repository/Login/Button_Masuk2'), 0)
	}

	@Then("User tap button back and tap button signin")
	public void user_tap_button_back_and_tap_button_signin() {
		Mobile.tap(findTestObject('Login/button_back'), 0)
		Mobile.tap(findTestObject('Login/Button_Masuk'), 0)
	}

	@Then("User input correct email")
	public void user_input_correct_email() {
		Mobile.setText(findTestObject('Login/fill_email'), 'zabyt@getnada.com', 0)
	}

	@Then("User input correct password")
	public void user_input_correct_password() {
		Mobile.setText(findTestObject('Login/fill_password'), 'Tremihely@31', 0)
	}

	@Then("User click button login")
	public void user_click_button_login() {
		Mobile.tap(findTestObject('Login/Button_Masuk2'), 0)
	}
}