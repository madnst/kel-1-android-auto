package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class profile {
	
	@Then("User tap button pencil")
	public void user_tap_button_pencil() {
	Mobile.tap(findTestObject('Profile/button_pencil'), 0)
	}
	
	@Then("User fill city")
	public void user_fill_city() {
	Mobile.tap(findTestObject('Profile/edittext_city'), 0)
	Mobile.setText(findTestObject('Profile/fill_city'), 'sleman', 0)
	Mobile.tap(findTestObject('Profile/Button_Simpan'), 0)
	}
	
	@Then("User fill address")
	public void user_fill_address() {
	Mobile.tap(findTestObject('Profile/edittext_address'), 0)
	Mobile.setText(findTestObject('Profile/fill_address'), 'jl damai', 0)
	Mobile.tap(findTestObject('Profile/Button_Simpan (1)'), 0)
	}
	
	@Then("User tap button back to profile")
	public void user_tap_button_back_to_profile() {
	Mobile.tap(findTestObject('Profile/button_backProfile'), 0)
	}
}