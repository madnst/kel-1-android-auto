package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class buy {
	@Then("User Tap button home")
	public void user_Tap_button_home() {
		Mobile.tap(findTestObject('Buy/button_Beranda'), 0)
	}

	@Then("User search product by categori")
	public void user_search_product_by_categori() {
		Mobile.tap(findTestObject('Buy/Button_Elektronik'), 0, FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User waiting delay")
	public void user_waiting_delay() {
		Mobile.delay(15, FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User tap product")
	public void user_tap_product() {
		Mobile.tap(findTestObject('Buy/product_Handphone'), 0)
	}

	@Then("User tap button negotiate")
	public void user_tap_button_negotiate() {
		Mobile.tap(findTestObject('Buy/Button_tertarik_Nego'), 0)
	}

	@Then("User fill price")
	public void user_fill_price() {
		Mobile.setText(findTestObject('Buy/fill_price'), '100000', 0)
	}

	@Then("tap button send")
	public void tap_button_send() {
		Mobile.tap(findTestObject('Buy/Button_Kirim'), 0)
	}

	@Then("tap button back_buyer")
	public void tap_button_back_buyer() {
		Mobile.tap(findTestObject('Buy/back_button_buy'), 0)
	}
}