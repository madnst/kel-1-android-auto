<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>Fill_noHP</name>
   <tag></tag>
   <elementGuidId>00000000-0000-0000-0000-000000000000</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>android.widget.EditText</value>
      <webElementGuid>0e8739f3-6f05-4d2e-8d39-55dc5457f6b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>index</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>5871d1d7-7581-4e29-809f-ab695abe2ab9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Contoh: 08123456789</value>
      <webElementGuid>7aa77725-452e-4bbc-aeb6-06384904a8b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>id.binar.fp.secondhand:id/et_phone</value>
      <webElementGuid>f451216d-73f1-4af9-9d2c-10e3da5a3b32</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>package</name>
      <type>Main</type>
      <value>id.binar.fp.secondhand</value>
      <webElementGuid>520326f4-45f7-48bf-96f3-ffdb045b08ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>checkable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>8408c01a-8e1a-4e5c-a1ee-99bb672386ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>checked</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>dd97bded-79cf-49cb-8f96-73a326311e35</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>clickable</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>db5039e2-666d-4702-9b2e-4dd0bb1cb43b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>enabled</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>2bfffd41-553f-4813-a8f2-da44721ca8ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>6dccb4e3-93ff-4aff-a3e3-0cb279b23628</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focused</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>340ef7a0-38ed-4609-bb0c-e6ede292cc10</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>scrollable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>4269e945-b5cf-4908-989a-1260b42cf253</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>long-clickable</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>3af09f8c-a2d0-4d1c-b72a-6c1e0648418e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>password</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>18d8f07f-3ee6-4dba-95be-2e4b2413bebd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>selected</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>22e14594-5c15-4c2d-938b-0f765e677d53</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x</name>
      <type>Main</type>
      <value>36</value>
      <webElementGuid>98ac21a7-e702-4438-b6c6-30a4ae0a6de0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>y</name>
      <type>Main</type>
      <value>1174</value>
      <webElementGuid>4a6bb4c8-ac67-4048-89b8-56a005be468a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>1008</value>
      <webElementGuid>88bfcc4d-ae2a-43f8-9474-e5b0d4fe21a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>116</value>
      <webElementGuid>8cb53a7a-3882-49ae-ba23-0f95d47c665b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>bounds</name>
      <type>Main</type>
      <value>[36,1174][1044,1290]</value>
      <webElementGuid>f4310361-2f83-427f-8891-aac2d7fc21d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>displayed</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>e20feaf1-4d41-4f49-9267-9b6bdbd9aafa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//hierarchy/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.widget.FrameLayout[1]/android.widget.ScrollView[1]/android.view.ViewGroup[1]/android.widget.LinearLayout[4]/android.widget.FrameLayout[1]/android.widget.EditText[1]</value>
      <webElementGuid>a961eeaf-e263-417b-bc19-24d92c71c83f</webElementGuid>
   </webElementProperties>
   <locator>//*[@class = 'android.widget.EditText' and (@text = 'Contoh: 08123456789' or . = 'Contoh: 08123456789') and @resource-id = 'id.binar.fp.secondhand:id/et_phone']</locator>
   <locatorStrategy>ATTRIBUTES</locatorStrategy>
</MobileElementEntity>
